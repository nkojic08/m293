let counter = document.getElementById("counter");
var number = 0;

// Überprüfen Sie beim Laden der Seite, ob im localStorage eine gespeicherte Anzahl vorhanden ist
window.addEventListener('DOMContentLoaded', () => {
    const savedCount = localStorage.getItem('cartItemCount');
    if (savedCount !== null) {
        number = parseInt(savedCount);
        counter.innerHTML = number;
    }
});

function shoppingcartplus() {
    number++;
    counter.innerHTML = number;
    
    localStorage.setItem('cartItemCount', number);
}

// Alertbox
function checkOut(){
    alert("Thank you for your order");
    // Zurücksetzen des Zählers
    number = 0;
    counter.innerHTML = number;
    localStorage.clear();
}
