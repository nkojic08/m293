let salads = [
    {
        name: "Green salad with tomatoe",
        prize: "4$",
        id: 1,
        ingredients: [
            "Iceberg lettuce",
            "Tomatoes"
        ],
        imageUrl: "./Bilder/GreenSaladWithTomato.jpg"
    },
    {
        name: "Tomato salad with mozzarella",
        prize: "5$",
        id: 2,
        ingredients: [
            "Tomato",
            "Mozzarella"
        ],
        imageUrl: "./Bilder/TomatoSaladWithMozzarella.jpg"
    },
    {
        name: "Field salad with egg",
        prize: "4$",
        id: 3,
        ingredients: [
            "Field salad",
            "Egg"
        ],
        imageUrl: "./Bilder/FieldSaladWithEgg.jpg"
    },
    {
        name: "Rocket with parmesan",
        prize: "5$",
        id: 4,
        ingredients: [
            "Rocket",
            "Parmesan"
        ],
        imageUrl: "./Bilder/RocketWithParmesan.jpg"
    }
]
for (var i = 0; i < salads.length; i++) {
    var saladName = salads[i].name;
    var ingredients = salads[i].ingredients;
    var prize = salads[i].prize;
    var id = salads[i].id;
    var image = salads[i].imageUrl;
    console.log(image)
    var saladtype = document.createElement('div');
    saladtype.className = 'saladType';
    saladtype.innerHTML = 
        '<img class="saladFotos" src="'+ image + '">' +
        '<div class="saladText">' + 
        '<h4>' + saladName + '</h4>' +
        '<p class="saladIngredients">' + ingredients + '</p>' + 
        '</div>' +
        '<div class="saladText-">' + 
        '<select name-"dressings">' + 
        '<option>' + "Italian dressing" + '</option>' + 
        '<option>' + "French dressing" + '</option>' +
        '</select>' +
        '<p class="prize">' + prize + '</p>' +
        '<button class="button" onclick="shoppingcartplus()">' +
        '<img class="shoppingCart" src="./Bilder/shoppingcart.png" alt="shopping-cart">' + 
        '</button>' +
        '</div>';
    document.getElementById(i + 1).appendChild(saladtype);
}