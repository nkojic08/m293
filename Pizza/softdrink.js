let softdrinks = [
    {
        name: "Coke",
        prize: "2$",
        id: 1,
        imageUrl: "./Bilder/coke.jpg",
        volume1: "33cl",
        volume2: "50cl",
        volume3: "1l"
    },
    {
        name: "Fanta",
        prize: "2$",
        id: 2,
        imageUrl: "./Bilder/fanta.jpg",
        volume1: "33cl",
        volume2: "50cl",
        volume3: "1l"
    },
    {
        name: "Pepsi",
        prize: "2$",
        id: 3,
        imageUrl: "./Bilder/pepsi.jpg",
        volume1: "33cl",
        volume2: "50cl",
        volume3: "1l"
    },
    {
        name: "Red bull",
        prize: "3$",
        id: 4,
        imageUrl: "./Bilder/redbull.jpg",
        volume1: "33cl",
        volume2: "50cl",
        volume3: "1l"
    }
]
for (var i = 0; i < softdrinks.length; i++) {
    var softdrinkName = softdrinks[i].name;
    var volume1 = softdrinks[i].volume1;
    var volume2 = softdrinks[i].volume2;
    var volume3 = softdrinks[i].volume3;
    var prize = softdrinks[i].prize;
    var id = softdrinks[i].id;
    var image = softdrinks[i].imageUrl;
    console.log(image)
    var softdrinktype = document.createElement('div');
    softdrinktype.className = 'soda';
    softdrinktype.innerHTML = 
        '<img class="sodaFoto" src="'+ image + '">' +
        '<p class="sodaText">' + softdrinkName + '</p>' +
        '<div class="buyingSoda">' + 
        '<select>' + 
        '<option value="0.33">' + volume1 + '</option>' + 
        '<option value="0.5">' + volume2 + '</option>' +
        '<option value="1">' + volume3 + '</option>' + 
        '</select>' +
        '<p class="prize">' + prize + '</p>' +
        '<button class="button" onclick="shoppingcartplus()">' +
        '<img class="shoppingCart" src="./Bilder/shoppingcart.png" alt="shopping-cart">' + 
        '</button>' +
        '</div>';
        
    document.getElementById(i + 1).appendChild(softdrinktype);
}