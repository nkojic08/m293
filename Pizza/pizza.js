let pizzas =  [
    {
        name: "Piccante",
        prize: "16$",
        id: 1,
        ingredients: [
            "Tomato", 
            " Mozzarella",
            " Spicy Salami",
            " Chilies",
            " Oregano"
        ],
        imageUrl: "./Bilder/piccante.jpg"
    }, 
    {
        name: "Giardino",
        prize: "14$",
        id: 2,
        ingredients: [
            "Tomato",
            " Mozzarella",
            " Artichokes",
            " Fresh Mushrooms"
        ],
        imageUrl: "./Bilder/giardino.jpg"
    }, 
    {
        name: "Prosciuotto e funghi",
        prize: "15$",
        id: 3,
        ingredients: [
            "Tomato",
            " Mozzarella",
            " Ham",
            " Fresh Mushrooms",
            " Oregano"
        ],
        imageUrl: "./Bilder/prosciuttoefunghi.jpg"
    }, 
    {
        name: "Quattro formaggi",
        prize: "13$",
        id: 4,
        ingredients: [
            "Tomato",
            " Mozzarella",
            " Parmesan",
            " Gorgonzola"
        ],
        imageUrl: "./Bilder/quattroFormaggi.jpg"
    }, 
    {
        name: "Quattro stagioni",
        prize: "17$",
        id: 5,
        ingredients: [
            "Tomato",
            " Mozzarella",
            " Ham",
            " Artichokes",
            " Fresh Mushrooms"
        ],
        imageUrl: "./Bilder/quattrostagioni.jpg"
    }, 
    {
        name: "Stromboli",
        prize: "12$",
        id: 6,
        ingredients: [
            " Tomato",
            " Mozzarella",
            " Fresh Chilies",
            " Olives",
            " Oregano"
        ],
        imageUrl: "./Bilder/stromboli.jpg"
    }, 
    {
        name: "Verde",
        prize: "13$",
        id: 7,
        ingredients: [
            " Tomato",
            " Mozzarella",
            " Broccoli",
            " Spinach",
            " Oregano"
        ],
        imageUrl: "./Bilder/verde.jpg"
    }, 
    {
        name: "Rustica",
        prize: "15$",
        id: 8,
        ingredients: [
            " Tomato",
            " Mozzarella",
            " Ham",
            " Bacon",
            " Onions",
            " Garlic",
            " Oregano"
        ],
        imageUrl: "./Bilder/rustica.jpg"
    }
]

for (var i = 0; i < pizzas.length; i++) {
    var pizzaName = pizzas[i].name;
    var ingredients = pizzas[i].ingredients;
    var prize = pizzas[i].prize;
    var id = pizzas[i].id;
    var image = pizzas[i].imageUrl;
    console.log(image)
    var pizzatype = document.createElement('div');
    pizzatype.className = 'pizzaType';
    pizzatype.innerHTML = 
        '<img class="pizzaFotos" src="'+ image + '">' +
        '<h4>' + pizzaName + '</h4>' +
        '<div class="pizzaText">' + 
        '<p class="pizzaIngredients">' + ingredients + '</p>' + 
        '<p class="prize">' + prize + '</p>' +
        '<button class="button" onclick="shoppingcartplus()">' +
        '<img class="shoppingCart" src="./Bilder/shoppingcart.png" alt="shopping-cart">' + 
        '</button>' +
        '</div>';
        
    document.getElementById(i + 1).appendChild(pizzatype);
}
