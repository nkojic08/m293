function getSize()
{
    var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    var height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
            
    document.getElementById("width").textContent = width;
    document.getElementById("height").textContent = height;
}
