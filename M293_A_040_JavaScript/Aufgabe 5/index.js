var clickCount = 0;
function changeColor()
{
  let btn1 = document.getElementById("Button1");
  let btn2 = document.getElementById("Button2");
  clickCount++;

  //Prüfen ob Button 1 rot eingefärbt ist und Button 2 nicht
  if(clickCount === 1){	  
    document.getElementById("Button1").style.backgroundColor = "red";
  }
  //Prüfen ob beide Buttons noch nicht eingefärbt sind
  if(clickCount === 2){
    document.getElementById("Button1").style.backgroundColor = "red";
    document.getElementById("Button2").style.backgroundColor = "red";
  }
}
