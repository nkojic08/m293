var counter = 0;

function updateCounter() {
    // Das HTML-Element aktualisieren, um den aktuellen Wert des Zählers anzuzeigen
    document.getElementById("counter").textContent = counter;
}

function incrementValue()
{
    counter++;
    updateCounter();
}

function decreaseValue()
{	
    if (counter > 0) {
    counter--;
    updateCounter();
    } else {
    // Falls der Zähler 0 ist oder darunter geht, Fehlermeldung ausgeben
    alert("Achtung, weniger als 0 geht nicht");
    }
}